import {Component, View} from 'angular2/core';

@Component({
  selector: 'githook-example'
})

@View({
  templateUrl: 'githook-example.html'
})

export class GithookExample {

  constructor() {
    console.info('GithookExample Component Mounted Successfully');
  }

}
