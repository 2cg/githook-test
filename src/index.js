import {Component, View} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {GithookExample} from 'githook-example';

@Component({
  selector: 'main'
})

@View({
  directives: [GithookExample],
  template: `
    <githook-example></githook-example>
  `
})

class Main {

}

bootstrap(Main);
